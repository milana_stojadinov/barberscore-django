# Barberscore

[![Build Status](https://semaphoreci.com/api/v1/dbinetti/barberscore-django/branches/master/badge.svg)](https://semaphoreci.com/dbinetti/barberscore-django)

[![Test Coverage](https://codeclimate.com/github/dbinetti/barberscore-django/badges/coverage.svg)](https://codeclimate.com/github/dbinetti/barberscore-django/coverage)

Score tracker for the Barbershop Harmony Society.

## Preface
The purpose of this project is to provide a simple, mobile-friendly app to track the performances at the International Convention of the BHS.

## Contributors
Big thank you to Alexander Boltenko with the Heralds of Harmony in Tampa, FL, who has hand-entered all the data and is keeping things up to date.  Thanks to @russkibass!

-r base.txt

pytest==3.0.6
pytest-django==3.1.2
factory-boy==2.8.1
faker==0.7.7
codeclimate-test-reporter==0.2.0
pytest-cov==2.4.0
